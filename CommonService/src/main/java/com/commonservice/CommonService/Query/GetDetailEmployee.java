package com.commonservice.CommonService.Query;

import lombok.Data;

@Data
public class GetDetailEmployee {
    private String employeeId ;
}
