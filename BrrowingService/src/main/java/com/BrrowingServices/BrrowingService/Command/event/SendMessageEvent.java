package com.BrrowingServices.BrrowingService.Command.event;

import lombok.Data;

@Data
public class SendMessageEvent {
    private  String Message ;
    private  String  Id ;
}
