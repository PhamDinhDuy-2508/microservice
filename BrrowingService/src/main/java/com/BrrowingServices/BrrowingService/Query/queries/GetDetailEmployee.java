package com.BrrowingServices.BrrowingService.Query.queries;

import lombok.Data;

@Data
public class GetDetailEmployee {
    private String employeeId ;
}
